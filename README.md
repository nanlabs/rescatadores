# Context
There is no unified place to go to when you are looking for a lost pet. Facebook
contains several pages but the functionality is not targeted to these ends, and
therefore the functionalities and its result are limited. Other projects tackle
the same problem but lack enough traction.

# Mission
We help users reunite with their lost pets by providing a platform where they can
share data of, and look for, their lost and found animal friends.

# Vision
No pet shall remain lost.

## MVP Scope
Allow users to create lost/found pets and see them in a common list. Data in this
platform is fresh, as it is kept alive for a limited amount of time. Users can
also see the detail of a posted pet and extend its lifespan.

## Extended scope
Users will be able to log in, therefore will also have a profile and extended
functionalities like personalized notifications and selection of areas of interest
in a map.