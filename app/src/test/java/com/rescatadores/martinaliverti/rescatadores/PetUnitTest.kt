package com.rescatadores.martinaliverti.rescatadores

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.rescatadores.martinaliverti.rescatadores.auth.AuthUIWrapper
import com.rescatadores.martinaliverti.rescatadores.auth.AuthViewModel
import com.rescatadores.martinaliverti.rescatadores.auth.Authenticator
import com.rescatadores.martinaliverti.rescatadores.pet.*
import com.rescatadores.martinaliverti.rescatadores.util.BitmapTransformation
import io.kotlintest.matchers.shouldBe
import io.kotlintest.specs.StringSpec
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import java.util.*

/**
 * Created by martinaliverti on 12/14/17.
 */

class PetUnitTest : StringSpec() {

    init {
        "PetViewModel should fetch data from the pet repository, when solicited data" {
            val testData : LiveData<List<Pet>> = MutableLiveData()
            val repository = Mockito.mock(PetRepository::class.java)
            val petFactory = Mockito.mock(PetFactory::class.java)
            val petViewModel = PetViewModel(repository, petFactory, PetHelper())
            `when`(repository.getAllPets()).thenReturn(testData)

            val data = petViewModel.getAllPets()

            verify(repository).getAllPets()
            data shouldBe testData
        }

        "PetViewModel should insert data in the pet repository, when asked to save data" {
            val repository = Mockito.mock(PetRepository::class.java)
            val creationDate = Date()
            val pet = Pet(
                    name = "munny",
                    description = "muy bonita su patita",
                    address = "somewhere 1234",
                    situation = "se perdió en el parque",
                    date = Date("27/03/1994"),
                    createdAt = creationDate,
                    phone = "2235348765",
                    owner = "juan carlos",
                    missing = false,
                    image = "url")
            val petFactory = Mockito.mock(PetFactory::class.java)
            `when`(petFactory.build(name = "munny",
                    description = "muy bonita su patita",
                    address = "somewhere 1234",
                    situation = "se perdió en el parque",
                    date = Date("27/03/1994"),
                    createdAt = creationDate,
                    phone = "2235348765",
                    owner = "juan carlos",
                    missing = false,
                    image = "")).thenReturn(pet)
            val petHelper = Mockito.mock(PetHelper::class.java)
            `when`(petHelper.getNewCreationDate()).thenReturn(creationDate)

            val petViewModel = PetViewModel(repository, petFactory, petHelper)
            petViewModel.postPet(name = "munny",
                    description = "muy bonita su patita",
                    address = "somewhere 1234",
                    situation = "se perdió en el parque",
                    date = Date("27/03/1994"),
                    phone = "2235348765",
                    owner = "juan carlos",
                    missing = false,
                    image = "")

            verify(repository).insertPet(pet)
            verify(petFactory).build(name = "munny",
                    description = "muy bonita su patita",
                    address = "somewhere 1234",
                    situation = "se perdió en el parque",
                    date = Date("27/03/1994"),
                    createdAt = creationDate,
                    phone = "2235348765",
                    owner = "juan carlos",
                    missing = false,
                    image = "")
        }

        "when pet created, user ID is added to it" {
            val authenticator = Mockito.mock(Authenticator::class.java)
            `when`(authenticator.getUserId()).thenReturn("id_test")
            val creationDate = Date()

            val petFactory = PetFactory(authenticator)
            val pet = petFactory.build(name = "munny",
                    description = "muy bonita su patita",
                    address = "somewhere 1234",
                    situation = "se perdió en el parque",
                    date = Date("27/03/1994"),
                    createdAt = creationDate,
                    phone = "2235348765",
                    owner = "juan carlos",
                    missing = false,
                    image = "")

            pet.userId shouldBe "id_test"
        }

        "when user is authenticated get pet creation intent" {
            val authenticator = Mockito.mock(Authenticator::class.java)
            `when`(authenticator.isAuthenticated()).thenReturn(true)

            val context = Mockito.mock(Context::class.java)
            val authUI = Mockito.mock(AuthUIWrapper::class.java)
            val generator = ActionIntentGenerator(context, authUI)
            val generatorSpy = Mockito.spy(generator)

            val userViewModel = AuthViewModel(authenticator, generatorSpy)

            userViewModel.getActionIntent()
            verify(generatorSpy).getPetCreationIntent()
        }

        "when user is not authenticated get sign in intent" {
            val authenticator = Mockito.mock(Authenticator::class.java)
            `when`(authenticator.isAuthenticated()).thenReturn(false)

            val context = Mockito.mock(Context::class.java)
            val authUI = Mockito.mock(AuthUIWrapper::class.java)
            val generator = ActionIntentGenerator(context, authUI)
            val generatorSpy = Mockito.spy(generator)

            val userViewModel = AuthViewModel(authenticator, generatorSpy)

            userViewModel.getActionIntent()
            verify(generatorSpy).getAuthenticationIntent()
        }

    }

}