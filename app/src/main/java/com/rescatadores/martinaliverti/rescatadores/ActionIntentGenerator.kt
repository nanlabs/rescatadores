package com.rescatadores.martinaliverti.rescatadores

import android.content.Context
import android.content.Intent
import com.rescatadores.martinaliverti.rescatadores.auth.AuthUIWrapper

/**
 * Created by martinaliverti on 1/10/18.
 */
class ActionIntentGenerator(val context: Context, val authUI: AuthUIWrapper) {

    fun getPetCreationIntent(): Intent {
        return Intent(context, NewPetActivity::class.java)
    }

    fun getAuthenticationIntent(): Intent {
        return authUI.getAuthIntent()
    }
}