package com.rescatadores.martinaliverti.rescatadores.pet

import android.arch.lifecycle.LiveData
import android.graphics.Bitmap

/**
 * Created by martinaliverti on 12/14/17.
 */
interface PetRepository {
    fun getAllPets(): LiveData<List<Pet>>
    fun insertPet(pet: Pet)
}