package com.rescatadores.martinaliverti.rescatadores.di

import com.google.firebase.auth.FirebaseAuth
import com.rescatadores.martinaliverti.rescatadores.ActionIntentGenerator
import com.rescatadores.martinaliverti.rescatadores.auth.AuthUIWrapper
import com.rescatadores.martinaliverti.rescatadores.auth.AuthViewModel
import com.rescatadores.martinaliverti.rescatadores.auth.Authenticator
import org.koin.android.module.AndroidModule

/**
 * Created by martinaliverti on 1/10/18.
 */
class AuthModule: AndroidModule() {
    override fun context() = applicationContext {
        context(name = "MainActivity") {
            provide { context }
            provide { Authenticator(get()) }
            provide { AuthUIWrapper() }
            provide { ActionIntentGenerator(get(), get()) }
            provide { AuthViewModel(get(), get()) }
            provide { FirebaseAuth.getInstance() }
        }

    }
}