package com.rescatadores.martinaliverti.rescatadores.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import java.io.ByteArrayOutputStream

/**
 * Created by martinaliverti on 1/11/18.
 */
class BitmapTransformation {

    companion object {
        private val maxWith = 1500
        private val maxHeight = 900
        private val compressQuality = 100

        fun bitmapToString(bitmap: Bitmap?): String {
            bitmap?.let {
                val baos = ByteArrayOutputStream()
                scaleDown(bitmap).compress(Bitmap.CompressFormat.PNG, compressQuality, baos)
                val byteArrayImage = baos.toByteArray()
                return Base64.encodeToString(byteArrayImage, Base64.DEFAULT)
            }
            return ""
        }

        private fun scaleDown(bitmap: Bitmap): Bitmap {
            var targetHeight = maxHeight
            var targetWidth = maxWith
            if (bitmap.height >= bitmap.width) {
                targetWidth = scaleValue(bitmap.width, bitmap.height, maxHeight)
            } else {
                targetHeight = scaleValue(bitmap.height, bitmap.width, maxWith)
            }
            return Bitmap.createScaledBitmap(bitmap, targetWidth, targetHeight, false)
        }

        private fun scaleValue(target: Int, reference: Int, max: Int): Int {
            return (target * (max.toFloat() / reference)).toInt()
        }

        fun stringToBitMap(serializedImage: String, width: Int, height: Int): Bitmap? {
            if (serializedImage.isNotEmpty()) {
                val decodedString = Base64.decode(serializedImage, Base64.DEFAULT)
                val options = BitmapFactory.Options()
                options.inSampleSize = calculateSampleSize(decodedString, width, height)
                return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size, options)
            }
            return null
        }

        private fun calculateSampleSize(decodedString: ByteArray, targetWidth: Int, targetHeight: Int): Int {
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size, options)
            val imageHeight = options.outHeight
            val imageWidth = options.outWidth

            var inSampleSize = 1

            if (imageHeight > targetHeight || imageWidth > targetWidth) {
                val halfHeight = imageHeight / 2
                val halfWidth = imageWidth / 2
                while (halfHeight / inSampleSize >= targetHeight && halfWidth / inSampleSize >= targetWidth) {
                    inSampleSize *= 2
                }
            }
            return inSampleSize
        }

    }

}