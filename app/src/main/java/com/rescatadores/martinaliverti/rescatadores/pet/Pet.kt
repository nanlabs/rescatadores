package com.rescatadores.martinaliverti.rescatadores.pet

import java.io.Serializable
import java.util.Date


/**
 * Created by martinaliverti on 12/14/17.
 */

data class Pet (
        var name: String = "",
        var description: String = "",
        var address: String = "",
        var situation: String = "",
        var date: Date,
        var createdAt: Date,
        var phone: String = "",
        var owner: String = "",
        var missing: Boolean = true,
        var image: String = "",
        var userId: String = ""
): Serializable {
    override fun toString(): String {
        return name
    }

}
