package com.rescatadores.martinaliverti.rescatadores.pet

import com.rescatadores.martinaliverti.rescatadores.auth.Authenticator
import java.util.*

/**
 * Created by martinaliverti on 12/15/17.
 */
class PetFactory(val authenticator: Authenticator) {
    fun build(name: String,
              description: String,
              address: String,
              situation: String,
              date: Date,
              createdAt: Date,
              phone: String,
              owner: String,
              missing: Boolean,
              image: String) =
            Pet(name = name,
                    description = description,
                    address = address,
                    situation = situation,
                    date = date,
                    createdAt = createdAt,
                    phone = phone,
                    owner = owner,
                    missing = missing,
                    image = image,
                    userId = authenticator.getUserId()!!)

}