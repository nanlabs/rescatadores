package com.rescatadores.martinaliverti.rescatadores.auth

import com.google.firebase.auth.FirebaseAuth

/**
 * Created by martinaliverti on 1/9/18.
 */
class Authenticator(val firebaseAuth: FirebaseAuth) {

    fun isAuthenticated(): Boolean {
        return firebaseAuth.currentUser != null
    }

    fun getUserId(): String? {
        return firebaseAuth.currentUser?.uid
    }

}