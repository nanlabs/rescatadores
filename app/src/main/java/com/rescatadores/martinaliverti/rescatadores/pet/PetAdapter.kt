package com.rescatadores.martinaliverti.rescatadores.pet

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.rescatadores.martinaliverti.rescatadores.PetDetailActivity
import com.rescatadores.martinaliverti.rescatadores.R
import com.rescatadores.martinaliverti.rescatadores.extensions.inflate
import com.rescatadores.martinaliverti.rescatadores.util.Screen

/**
 * Created by martinaliverti on 12/15/17.
 */
class PetAdapter: RecyclerView.Adapter<PetViewHolder>() {

    var pets: List<Pet> = mutableListOf()

    override fun onBindViewHolder(holder: PetViewHolder, position: Int) {
        holder.bind(pets[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, PetDetailActivity::class.java)
            val pet = pets[position].copy(image = "")
            intent.putExtra("pet", pet)
            it.context.getSharedPreferences("pets", 0).edit()
                    .putString("image", pets[position].image).apply()
            it.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int = pets.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PetViewHolder {
        return PetViewHolder(parent.inflate(R.layout.pet_item), Screen(parent.context))
    }
}