package com.rescatadores.martinaliverti.rescatadores

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rescatadores.martinaliverti.rescatadores.databinding.ActivityPetDetailBinding
import com.rescatadores.martinaliverti.rescatadores.extensions.getHeightFor
import com.rescatadores.martinaliverti.rescatadores.pet.Pet
import com.rescatadores.martinaliverti.rescatadores.util.BitmapTransformation
import com.rescatadores.martinaliverti.rescatadores.util.Screen
import kotlinx.android.synthetic.main.activity_pet_detail.*
import org.koin.android.ext.android.inject
import java.util.*


class PetDetailActivity : AppCompatActivity() {

    private val screen by inject<Screen>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pet_detail)
        val binding: ActivityPetDetailBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_pet_detail)
        binding.pet = intent.getSerializableExtra("pet") as Pet
        setImage()
        setDate(binding.pet!!.date)
        setStateBadge(binding.pet!!.missing)
    }

    private fun setStateBadge(missing: Boolean) {
        petState.text = resources.getString(getStringResource(missing))
        petState.setBackgroundColor(resources.getColor(getColorResource(missing)))
    }

    private fun getColorResource(missing: Boolean): Int {
        return if (missing) R.color.lost else R.color.found
    }

    private fun getStringResource(missing: Boolean): Int {
        return if (missing) R.string.pet_state_lost else R.string.pet_state_found
    }

    private fun setImage() {
        val image = getSharedPreferences("pets", 0).getString("image", "")
        val screenWidth = screen.getResolution().x
        val bitmap = BitmapTransformation.stringToBitMap(image, screenWidth, imageViewPet.getHeightFor(screenWidth))
        imageViewPet.setImageBitmap(bitmap)
    }

    private fun setDate(incidentDate: Date) {
        val dateFormat = android.text.format.DateFormat.getDateFormat(applicationContext)
        date.text = dateFormat.format(incidentDate)
    }

}
