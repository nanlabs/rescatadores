package com.rescatadores.martinaliverti.rescatadores

import android.app.Application
import com.rescatadores.martinaliverti.rescatadores.di.AuthModule
import com.rescatadores.martinaliverti.rescatadores.di.PetModule
import org.koin.android.ext.android.startKoin

/**
 * Created by martinaliverti on 12/15/17.
 */
class MainApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, getModules())
    }

    private fun getModules() = listOf(PetModule(), AuthModule())
}