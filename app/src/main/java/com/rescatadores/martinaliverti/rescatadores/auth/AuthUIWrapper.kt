package com.rescatadores.martinaliverti.rescatadores.auth

import android.content.Intent
import com.firebase.ui.auth.AuthUI
import java.util.*

/**
 * Created by martinaliverti on 1/10/18.
 */
class AuthUIWrapper {

    var providers = Arrays.asList(
            AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
            AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build())

    fun getAuthIntent(): Intent {
        return AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build()
    }

}