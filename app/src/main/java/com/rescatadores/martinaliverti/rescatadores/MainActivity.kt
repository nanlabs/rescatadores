package com.rescatadores.martinaliverti.rescatadores

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.rescatadores.martinaliverti.rescatadores.auth.AuthViewModel
import com.rescatadores.martinaliverti.rescatadores.pet.PetAdapter
import com.rescatadores.martinaliverti.rescatadores.pet.PetViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.standalone.KoinComponent


class MainActivity : AppCompatActivity(), KoinComponent {

    private val petViewModel by inject<PetViewModel>()
    private val petAdapter by inject<PetAdapter>()
    private val authViewModel by inject<AuthViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpAddButtonBehavior()
        initRecycler()
        petViewModel.getAllPets().observe(this, Observer {
            it?.let {
                petAdapter.pets = it
                petAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun setUpAddButtonBehavior() {
        addPet.setOnClickListener({
            startActivity(authViewModel.getActionIntent())
        })
    }

    private fun initRecycler() {
        petsList.layoutManager = LinearLayoutManager(this)
        petsList.adapter = petAdapter
    }

}