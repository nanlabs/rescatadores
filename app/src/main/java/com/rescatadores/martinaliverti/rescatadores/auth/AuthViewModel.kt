package com.rescatadores.martinaliverti.rescatadores.auth

import android.content.Intent
import com.rescatadores.martinaliverti.rescatadores.ActionIntentGenerator

/**
 * Created by martinaliverti on 1/10/18.
 */
class AuthViewModel(val authenticator: Authenticator,
                    val generator: ActionIntentGenerator) {

    fun getActionIntent(): Intent {
        return if (authenticator.isAuthenticated()) {
            generator.getPetCreationIntent()
        } else {
            generator.getAuthenticationIntent()
        }
    }

}