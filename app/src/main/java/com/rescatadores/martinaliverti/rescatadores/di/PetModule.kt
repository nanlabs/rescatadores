package com.rescatadores.martinaliverti.rescatadores.di

import android.arch.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.rescatadores.martinaliverti.rescatadores.pet.*
import com.rescatadores.martinaliverti.rescatadores.util.Screen
import org.koin.android.module.AndroidModule


/**
 * Created by martinaliverti on 12/15/17.
 */
class PetModule: AndroidModule() {
    override fun context() = applicationContext {
        context(name = "MainActivity") {
            provide { Screen(get()) }
            provide { PetFactory(get()) }
            provide { MutableLiveData<List<Pet>>() }
            provide { PetHelper() }
            provide { PetAdapter() }
            provide { FirebaseFirestore.getInstance() }
            provide { PetViewModel(get(), get(), get()) }
            provide { FirestorePetRepository(get(), get()) } bind PetRepository::class
        }

    }
}