package com.rescatadores.martinaliverti.rescatadores.extensions

import com.google.android.gms.tasks.Task
import kotlin.coroutines.experimental.suspendCoroutine

/**
 * Created by martinaliverti on 12/27/17.
 */

suspend fun <T> Task<T>.await(): T = suspendCoroutine { continuation ->
    addOnCompleteListener { task ->
        continuation.resume(task.result)
    }
}