package com.rescatadores.martinaliverti.rescatadores.pet

import java.util.Date

/**
 * Created by martinaliverti on 12/14/17.
 */
class PetViewModel constructor(val repository : PetRepository,
                               val petFactory: PetFactory,
                               val petHelper: PetHelper) {

    fun getAllPets() = repository.getAllPets()

    fun postPet(name: String,
                description: String,
                address: String,
                situation: String,
                date: Date,
                phone: String,
                owner: String,
                missing: Boolean,
                image: String) =
            repository.insertPet(petFactory.build(name,
                    description,
                    address,
                    situation,
                    date,
                    petHelper.getNewCreationDate(),
                    phone,
                    owner,
                    missing,
                    image))

}