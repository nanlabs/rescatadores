package com.rescatadores.martinaliverti.rescatadores.pet

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.rescatadores.martinaliverti.rescatadores.extensions.await
import kotlinx.coroutines.experimental.launch
import java.util.*

/**
 * Created by martinaliverti on 12/20/17.
 */
class FirestorePetRepository constructor (val appDataBase: FirebaseFirestore,
                                          var pets: MutableLiveData<List<Pet>>): PetRepository {

    override fun getAllPets(): LiveData<List<Pet>> {
        if (pets.value == null) {
            val docRef = appDataBase.collection("pets")
            docRef.addSnapshotListener( { snapshot, _ ->
                if (snapshot != null) {
                    pets.value = getPetsFromSnapshot(snapshot)
                }
            })
        }
        return pets
    }

    private fun getPetsFromSnapshot(snapshot: QuerySnapshot): List<Pet> {
        val list = mutableListOf<Pet>()
        snapshot.documents.mapTo(list) {
            Pet(it.get("name") as String,
                    it.get("description") as String,
                    it.get("address") as String,
                    it.get("situation") as String,
                    it.get("date") as Date,
                    it.get("createdAt") as Date,
                    it.get("phone") as String,
                    it.get("owner") as String,
                    it.get("missing") as Boolean,
                    it.get("image") as String,
                    it.get("userId") as String)
        }
        list.sortByDescending { it.createdAt }
        return list
    }

    override fun insertPet(pet: Pet) {
        launch {
            appDataBase.collection("pets").add(pet).await()
        }
    }
}