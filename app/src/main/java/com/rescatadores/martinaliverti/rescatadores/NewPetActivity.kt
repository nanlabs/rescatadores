package com.rescatadores.martinaliverti.rescatadores

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.rescatadores.martinaliverti.rescatadores.pet.PetViewModel
import kotlinx.android.synthetic.main.activity_new_pet.*
import org.koin.android.ext.android.inject
import android.view.inputmethod.EditorInfo
import android.content.Intent
import android.widget.EditText
import android.app.DatePickerDialog
import java.util.*
import java.text.SimpleDateFormat
import android.view.inputmethod.InputMethodManager
import android.widget.CompoundButton
import com.rescatadores.martinaliverti.rescatadores.extensions.getBitmap
import com.rescatadores.martinaliverti.rescatadores.util.BitmapTransformation
import com.squareup.picasso.Picasso


class NewPetActivity : AppCompatActivity() {

    private val petViewModel by inject<PetViewModel>()
    private val IMAGE_UPDATED: String = "updated"
    private var calendar = Calendar.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_pet)
        setUpViewsBehaviors()
    }

    private fun setUpViewsBehaviors() {
        owner.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                insertPet(view)
                true
            } else {
                false
            }
        }
        date.setOnClickListener({
            openDatePickerDialog()
        })
        date.setOnFocusChangeListener({ _: View, focused: Boolean ->
            if (focused) openDatePickerDialog()
        })
        switchLostFound.setOnCheckedChangeListener({ _: CompoundButton, checked: Boolean ->
            if (checked)
                labelAddress.setText(R.string.report_pet_address_message_found)
            else
                labelAddress.setText(R.string.report_pet_address_message_lost)
        })
    }

    fun openDatePickerDialog() {
        hideKeyboard()
        val datePicker = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }
        val dialog = DatePickerDialog(this,
                datePicker,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
        dialog.datePicker.maxDate = Date().time
        dialog.show()
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun insertPet(view: View) {
        if (fieldsAreValid()) {
            petViewModel.postPet(name.text.toString(),
                description.text.toString(),
                address.text.toString(),
                situation.text.toString(),
                Date(date.text.toString()),
                phone.text.toString(),
                owner.text.toString(),
                !switchLostFound.isChecked,
                BitmapTransformation.bitmapToString(imagePet.getBitmap()))
            finish()
        }
    }

    private fun fieldsAreValid(): Boolean {
        return fieldIsValid(name, "Please provide a name") and
                fieldIsValid(description, "Please provide a description") and
                fieldIsValid(address, "Please provide an address") and
                fieldIsValid(date, "Please provide a date") and
                fieldIsValid(phone, "Please provide a contact") and
                imageIsSet()
    }

    //TODO: this function does 2 things, validates filed is set, and shows/hides error, but the name suggests just one. Refactor
    private fun fieldIsValid(field: EditText, message: String): Boolean {
        if (field.text.isEmpty()) {
            field.error = message
            return false
        }
        return true
    }

    //TODO: this function does 2 things, validates image is set, and shows/hides badge, but the name suggests just one. Refactor
    private fun imageIsSet(): Boolean {
        if (imagePet.tag != IMAGE_UPDATED) {
            imageMissingBadge.visibility = View.VISIBLE
            return false
        }
        return true
    }

    fun openGallery(view: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    private fun updateLabel() {
        val myFormat = "dd/MM/yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        date.setText(sdf.format(calendar.time))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (resultCode == Activity.RESULT_OK && intent != null) {
            Picasso.with(this).load(intent.data.toString()).fit().centerInside().into(imagePet)
            imageMissingBadge.visibility = View.GONE
            imagePet.tag = IMAGE_UPDATED
        }
        super.onActivityResult(requestCode, resultCode, intent)
    }
}
