package com.rescatadores.martinaliverti.rescatadores.pet

import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.rescatadores.martinaliverti.rescatadores.R
import com.rescatadores.martinaliverti.rescatadores.extensions.getHeightFor
import com.rescatadores.martinaliverti.rescatadores.util.BitmapTransformation
import com.rescatadores.martinaliverti.rescatadores.util.Screen
import kotlinx.android.synthetic.main.pet_item.view.*

/**
 * Created by martinaliverti on 12/15/17.
 */
class PetViewHolder(itemView: View, val screen: Screen): RecyclerView.ViewHolder(itemView) {

    fun bind(pet: Pet) = with(itemView) {
        petName.text = pet.name
        textViewDescription.text = pet.description
        textViewAddress.text = pet.address
        textViewPhone.text = pet.phone
        setImage(pet, petImage)
        setStateBadge(pet, petState, resources)
    }

    private fun setStateBadge(pet: Pet, petState: TextView, resources: Resources) {
        petState.text = resources.getString(getStringResource(pet.missing))
        petState.setBackgroundColor(resources.getColor(getColorResource(pet.missing)))
    }

    private fun getColorResource(missing: Boolean): Int {
        return if (missing) R.color.lost else R.color.found
    }

    private fun getStringResource(missing: Boolean): Int {
        return if (missing) R.string.pet_state_lost else R.string.pet_state_found
    }

    private fun setImage(pet: Pet, petImage: ImageView) {
        val screenWidth = screen.getResolution().x
        petImage.context
        when {
            pet.image.isNotEmpty() -> {
                val bitmap = BitmapTransformation.stringToBitMap(pet.image, screenWidth, petImage.getHeightFor(screenWidth))
                petImage.setImageBitmap(bitmap)
            }
            else -> petImage.setImageResource(R.drawable.ic_photo_camera_black_36dp)
        }
    }

}