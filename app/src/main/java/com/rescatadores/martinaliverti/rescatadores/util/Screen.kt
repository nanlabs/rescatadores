package com.rescatadores.martinaliverti.rescatadores.util

import android.content.Context
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

/**
 * Created by martinaliverti on 1/12/18.
 */
class Screen(val context: Context) {

    fun getResolution(): Point {
        val wm = context.getSystemService(AppCompatActivity.WINDOW_SERVICE) as WindowManager
        val resolution = Point()
        wm.defaultDisplay.getRealSize(resolution)
        return resolution
    }

}