package com.rescatadores.martinaliverti.rescatadores.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by martinaliverti on 12/15/17.
 */
fun ViewGroup.inflate(layoutRes: Int): View = LayoutInflater.from(context)
        .inflate(layoutRes, this, false)