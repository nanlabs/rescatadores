package com.rescatadores.martinaliverti.rescatadores.extensions

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.widget.ImageView

/**
 * Created by martinaliverti on 1/11/18.
 */
val heightToWidthRatio = 0.7

fun ImageView.getBitmap(): Bitmap = (this.drawable as BitmapDrawable).bitmap

fun ImageView.getHeightFor(width: Int): Int = (width * heightToWidthRatio).toInt()