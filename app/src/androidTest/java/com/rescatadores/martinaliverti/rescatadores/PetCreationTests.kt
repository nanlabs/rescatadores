package com.rescatadores.martinaliverti.rescatadores

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.intent.Intents.intending
import android.app.Activity
import android.app.Instrumentation.ActivityResult
import android.net.Uri
import android.support.test.espresso.Espresso
import android.support.test.espresso.intent.matcher.IntentMatchers.isInternal
import com.rescatadores.martinaliverti.rescatadores.EspressoHelper.Companion.atPosition
import com.rescatadores.martinaliverti.rescatadores.EspressoHelper.Companion.showsError
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.contrib.PickerActions
import android.widget.DatePicker
import android.support.test.espresso.matcher.ViewMatchers.withClassName
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Matchers


/**
 * Created by nanlabs on 13/12/17.
 */

@RunWith(AndroidJUnit4::class)
class PetCreationTests {

    @Rule @JvmField
    val mainActivity = IntentsTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun petCardAdded() {
        Thread.sleep(500) // firestore is async and takes time to fill in data to the LiveData
        val childCount = mainActivity.activity.findViewById<RecyclerView>(R.id.petsList).adapter.itemCount
        addLostPet()
        assertThat(mainActivity.activity.findViewById<RecyclerView>(R.id.petsList).adapter.itemCount, equalTo(childCount + 1))
    }

    @Test
    fun newLostPetCardShowsPetInfo() {
        addLostPet()
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("firulai")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("descripcion")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("anywhere 247")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("22215436767")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("Lost")))))
    }

    @Test
    fun newFoundPetCardShowsPetInfo() {
        addFoundPet()
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("firulai")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("descripcion")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("anywhere 247")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("22215436767")))))
        onView(withId(R.id.petsList)).check(matches(atPosition(0, hasDescendant(withText("Found")))))
    }

    private fun addLostPet() {
        addPet(true)
    }

    private fun addFoundPet() {
        addPet(false)
    }

    private fun addPet(missing: Boolean) {
        onView(withId(R.id.addPet)).perform(click())
        selectImage()
        if (!missing) onView(withId(R.id.switchLostFound)).perform(click())
        onView(withId(R.id.name)).perform(typeText("firulai"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.description)).perform(typeText("descripcion"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.address)).perform(typeText("anywhere 247"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.situation)).perform(typeText("situacion"))
        Espresso.closeSoftKeyboard()
        pickDate(2014, 2, 19)
        onView(withId(R.id.phone)).perform(typeText("22215436767"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.owner)).perform(typeText("Juan Carlos"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.save)).perform(click())
    }

    private fun selectImage() {
        val uri = Uri.parse("android.resource://com.rescatadores.martinaliverti.rescatadores/drawable/ic_launcher_test")
        val resultData = Intent()
        resultData.data = uri
        val result = ActivityResult(Activity.RESULT_OK, resultData)
        intending(not(isInternal())).respondWith(result)
        onView(withId(R.id.imagePet)).perform(click())
    }

    private fun pickDate(year: Int, month: Int, day: Int) {
        onView(withId(R.id.date)).perform(scrollTo(), click())
        onView(withClassName(Matchers.equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(year, month + 1, day))
        onView(withId(android.R.id.button1)).perform(click())
    }

    @Test
    fun imageGalleryOpenedAndImageDisplayed() {
        onView(withId(R.id.addPet)).perform(click())
        selectImage()
        //TODO: Find a way to validate the imageView displays the correct image
        onView(withId(R.id.imagePet)).check(matches(isDisplayed()))
    }

    @Test
    fun datePickerOpenedAndFieldPopulated() {
        onView(withId(R.id.addPet)).perform(click())
        pickDate(2018, 6, 14)
        onView(withId(R.id.date)).check(matches(withText("14/07/2018")))
    }

    @Test
    fun mandatoryFieldsShowErrorHintWhenIncomplete() {
        onView(withId(R.id.addPet)).perform(click())
        onView(withId(R.id.save)).perform(click())

        onView(withId(R.id.name)).check(matches(showsError()))
        onView(withId(R.id.date)).check(matches(showsError()))
        onView(withId(R.id.description)).check(matches(showsError()))
        onView(withId(R.id.address)).check(matches(showsError()))
        onView(withId(R.id.phone)).check(matches(showsError()))
    }

    @Test
    fun whenNoImageSetShowMissingImageBadge() {
        onView(withId(R.id.addPet)).perform(click())
        onView(withId(R.id.save)).perform(click())
        onView(withId(R.id.imageMissingBadge)).check(matches(isDisplayed()))
    }

    @Test
    fun whenCreatingPetShowCorrectAddressMessage() {
        onView(withId(R.id.addPet)).perform(click())
        onView(withId(R.id.labelAddress)).check(matches(withText("Where did you last see him/her?")))
        onView(withId(R.id.switchLostFound)).perform(click())
        onView(withId(R.id.labelAddress)).check(matches(withText("Where did you find him/her")))
    }

}