package com.rescatadores.martinaliverti.rescatadores

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import android.widget.EditText


/**
 * Created by martinaliverti on 12/29/17.
 */
class EspressoHelper {

    companion object {
        fun withDrawable(expectedResourceId: Int) : Matcher<View> {
            return DrawableMatcher(expectedResourceId)
        }

        fun noDrawable(): Matcher<View> {
            return DrawableMatcher(-1)
        }


        fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
            checkNotNull(itemMatcher)
            return RecyclerViewPositionMatcher(position, itemMatcher)
        }

        fun showsError(): Matcher<View> {
            return ShowsErrorMatcher()
        }
    }

    class RecyclerViewPositionMatcher(val position: Int, val itemMatcher: Matcher<View>) :
            BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder = view.findViewHolderForAdapterPosition(position) ?: // has no item on such position
                    return false
            return itemMatcher.matches(viewHolder.itemView)
        }
    }

    class DrawableMatcher(val expectedResourceId: Int) : TypeSafeMatcher<View>() {
        override fun describeTo(description: Description) {}

        override fun matchesSafely(item: View): Boolean {
            if (item !is ImageView) return false
            if (expectedResourceId < 0) return item.drawable == null
            val resources = item.getContext().resources
            val expectedDrawable = resources.getDrawable(expectedResourceId) ?: return false
            val bitmap = getBitmap(item.drawable)
            val otherBitmap = getBitmap(expectedDrawable)
            return bitmap!!.sameAs(otherBitmap)
        }

        fun getBitmap(drawable: Drawable?): Bitmap? {
            val bitmap = Bitmap.createBitmap(
                    drawable!!.intrinsicWidth,
                    drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            return bitmap
        }
    }

    class ShowsErrorMatcher : TypeSafeMatcher<View>() {
        override fun describeTo(description: Description?) {
        }

        override fun matchesSafely(item: View?): Boolean {
            return !(item as EditText).error.isNullOrEmpty()
        }

    }
}