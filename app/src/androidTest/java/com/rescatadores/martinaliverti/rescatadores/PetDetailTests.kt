package com.rescatadores.martinaliverti.rescatadores

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.PickerActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers
import android.support.v7.widget.RecyclerView
import android.widget.DatePicker
import com.rescatadores.martinaliverti.rescatadores.pet.Pet
import com.rescatadores.martinaliverti.rescatadores.pet.PetAdapter
import com.rescatadores.martinaliverti.rescatadores.pet.PetViewHolder
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test

/**
 * Created by martinaliverti on 1/12/18.
 */
class PetDetailTests {

    @Rule
    @JvmField
    val mainActivity = IntentsTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun lostPetInformationIsDisplayed() {
        addLostPet()
        Thread.sleep(5000) // firestore is async and takes time to fill in data to the LiveData, especially with big images
        val pet = getFirstPet()
        onView(ViewMatchers.withId(R.id.petsList)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PetViewHolder>(0, ViewActions.click())
        )

        onView(ViewMatchers.withId(R.id.name)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.name)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.description)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.description)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.address)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.address)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.situation)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.situation)).check(matches(ViewMatchers.isDisplayed()))

        val dateString = android.text.format.DateFormat.getDateFormat(mainActivity.activity).format(pet.date)
        onView(ViewMatchers.withId(R.id.date)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(dateString)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.phone)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.phone)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.owner)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.owner)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.petState)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.petState)).check(matches(ViewMatchers.withText("Lost")))
    }

    @Test
    fun foundPetInformationIsDisplayed() {
        addFoundPet()
        Thread.sleep(5000) // firestore is async and takes time to fill in data to the LiveData, especially with big images
        val pet = getFirstPet()
        onView(ViewMatchers.withId(R.id.petsList)).perform(
                RecyclerViewActions.actionOnItemAtPosition<PetViewHolder>(0, ViewActions.click())
        )

        onView(ViewMatchers.withId(R.id.name)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.name)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.description)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.description)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.address)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.address)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.situation)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.situation)).check(matches(ViewMatchers.isDisplayed()))

        val dateString = android.text.format.DateFormat.getDateFormat(mainActivity.activity).format(pet.date)
        onView(ViewMatchers.withId(R.id.date)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(dateString)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.phone)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.phone)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.owner)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withText(pet.owner)).check(matches(ViewMatchers.isDisplayed()))

        onView(ViewMatchers.withId(R.id.petState)).check(matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.petState)).check(matches(ViewMatchers.withText("Found")))
    }

    private fun getFirstPet(): Pet {
        val petAdapter = mainActivity.activity.findViewById<RecyclerView>(R.id.petsList).adapter as PetAdapter
        return petAdapter.pets[0]
    }

    private fun addLostPet() {
        addPet(true)
    }

    private fun addFoundPet() {
        addPet(false)
    }

    private fun addPet(missing: Boolean) {
        onView(ViewMatchers.withId(R.id.addPet)).perform(ViewActions.click())
        selectImage()
        if (!missing) onView(ViewMatchers.withId(R.id.switchLostFound)).perform(ViewActions.click())
        onView(ViewMatchers.withId(R.id.name)).perform(ViewActions.typeText("firulai"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.description)).perform(ViewActions.typeText("descripcion"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.address)).perform(ViewActions.typeText("anywhere 247"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.situation)).perform(ViewActions.typeText("situacion"))
        Espresso.closeSoftKeyboard()
        pickDate(2014, 2, 19)
        onView(ViewMatchers.withId(R.id.phone)).perform(ViewActions.typeText("22215436767"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.owner)).perform(ViewActions.typeText("Juan Carlos"))
        Espresso.closeSoftKeyboard()
        onView(ViewMatchers.withId(R.id.save)).perform(ViewActions.click())
    }

    private fun selectImage() {
        val uri = Uri.parse("android.resource://com.rescatadores.martinaliverti.rescatadores/drawable/ic_launcher_test")
        val resultData = Intent()
        resultData.data = uri
        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal())).respondWith(result)
        onView(ViewMatchers.withId(R.id.imagePet)).perform(ViewActions.click())
    }

    private fun pickDate(year: Int, month: Int, day: Int) {
        onView(ViewMatchers.withId(R.id.date)).perform(ViewActions.scrollTo(), ViewActions.click())
        onView(ViewMatchers.withClassName(Matchers.equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(year, month + 1, day))
        onView(ViewMatchers.withId(android.R.id.button1)).perform(ViewActions.click())
    }
}