package com.rescatadores.martinaliverti.rescatadores

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.PickerActions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import android.widget.DatePicker
import com.rescatadores.martinaliverti.rescatadores.pet.PetViewHolder
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by martinaliverti on 12/28/17.
 */

@RunWith(AndroidJUnit4::class)
class NavigationTests {

    @Rule
    @JvmField
    val mainActivity = IntentsTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun transitionToNewPetActivity() {
        onView(withId(R.id.addPet)).perform(ViewActions.click())
        onView(withId(R.id.save)).check(matches(isDisplayed()))
    }

    @Test
    fun transitionToPetDetailView() {
        addPet()
        Thread.sleep(5000) // firestore is async and takes time to fill in data to the LiveData, especially with big images
        Espresso.onView(ViewMatchers.withId(R.id.petsList))
                .perform(RecyclerViewActions.actionOnItemAtPosition<PetViewHolder>(0, ViewActions.click()))
        onView(withId(R.id.imageViewPet)).check(matches(isDisplayed()))
    }

    private fun addPet() {
        onView(withId(R.id.addPet)).perform(ViewActions.click())
        selectImage()
        onView(withId(R.id.name)).perform(ViewActions.typeText("firulai"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.description)).perform(ViewActions.typeText("descripcion"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.address)).perform(ViewActions.typeText("anywhere 247"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.situation)).perform(ViewActions.typeText("situacion"))
        Espresso.closeSoftKeyboard()
        pickDate(2014, 2, 19)
        onView(withId(R.id.phone)).perform(ViewActions.typeText("22215436767"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.owner)).perform(ViewActions.typeText("Juan Carlos"))
        Espresso.closeSoftKeyboard()
        onView(withId(R.id.save)).perform(ViewActions.click())
    }

    private fun selectImage() {
        val uri = Uri.parse("android.resource://com.rescatadores.martinaliverti.rescatadores/drawable/ic_launcher_test")
        val resultData = Intent()
        resultData.data = uri
        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)
        Intents.intending(CoreMatchers.not(IntentMatchers.isInternal())).respondWith(result)
        onView(withId(R.id.imagePet)).perform(ViewActions.click())
    }

    private fun pickDate(year: Int, month: Int, day: Int) {
        onView(withId(R.id.date)).perform(ViewActions.scrollTo(), ViewActions.click())
        onView(withClassName(Matchers.equalTo(DatePicker::class.java.name))).perform(PickerActions.setDate(year, month + 1, day))
        onView(withId(android.R.id.button1)).perform(ViewActions.click())
    }
}